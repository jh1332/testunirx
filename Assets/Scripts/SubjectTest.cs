﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class SubjectTest : MonoBehaviour
{
	private void Awake()
	{
		Subject<string> subject = new Subject<string>();

		subject.Subscribe(msg => Debug.Log("Subscribe1: " + msg));
		subject.Subscribe(msg => Debug.Log("Subscribe2: " + msg));
		subject.Subscribe(msg => Debug.Log("Subscribe3: " + msg));

		subject.OnNext("Hello");
		subject.OnNext("Hi");
	}
}
